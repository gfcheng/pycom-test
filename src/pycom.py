#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 2011-10-29

@author: goldfilm(at)gmail.com
'''
import wx
import serial
#import sys
import threading
import time
import binascii
class WinMain(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, u"cgf's com shell",
            wx.DefaultPosition, wx.Size(500, 400), wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.TX_count = 0   #发送计数
        self.RX_count = 0   #接收计数
        self.is_linking_to_port = False
        # 循环扫描可用COM端
        #for i in range(256):
            #try:
                #s = serial.Serial(i)
                #self.serial_ports.append([i, s.portstr])
                #s.close()
                #break
            #except serial.SerialException:
                #pass
        self.serial = serial.Serial()

        self.serial_ports = ('COM1','COM2','COM3','COM4','COM5')
        self.serial_port_dict = self.List_To_Dict(self.serial_ports)
        self.serial_baudrate_dict =self.List_To_Dict(self.serial.BAUDRATES)
        self.serial_bytesize_dict =self.List_To_Dict(self.serial.BYTESIZES)
        self.serial_stopbits_dict= self.List_To_Dict(self.serial.STOPBITS)
        self.serial_parity_dict = self.List_To_Dict(self.serial.PARITIES)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        sizer_top = wx.BoxSizer(wx.VERTICAL)
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        sizer_main = wx.BoxSizer(wx.VERTICAL)

        # 布置组件

        # 接收显示
        self.recv_text = wx.TextCtrl(self.panel, wx.ID_ANY,
            u"",wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE)
        sizer_main.Add(self.recv_text, 1, wx.ALL | wx.EXPAND, 5)

        # option 1
        sizer_option_1 = wx.BoxSizer(wx.HORIZONTAL)

        self.option_1_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'输入框：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_1.Add(self.option_1_st1, 0, wx.ALL, 5)

        self.send_text = wx.TextCtrl(self.panel, wx.ID_ANY,
            u'',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_1.Add(self.send_text, 1, wx.ALL, 5)

        # 发送按钮
        self.send_button = wx.Button(self.panel, wx.ID_ANY,
            u'发送', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_1.Add(self.send_button, 0, wx.ALL, 5)

        # 清除按钮
        self.clear_button = wx.Button(self.panel, wx.ID_ANY,
            u'清除', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_1.Add(self.clear_button, 0, wx.ALL, 5)

        self.display_as_hex = wx.CheckBox(self.panel, wx.ID_ANY,
            u'HEX显示',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_1.Add(self.display_as_hex, 0, wx.ALL, 5)

        sizer_main.Add(sizer_option_1, 0, wx.ALL | wx.EXPAND, 5)

        # option 2
        sizer_option_2 = wx.BoxSizer(wx.HORIZONTAL)

        # 设置串口号
        self.option_2_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'串口号：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_2.Add(self.option_2_st1, 0, wx.ALL, 5)

        self.option_port = wx.Choice(self.panel, wx.ID_ANY,
             wx.DefaultPosition, wx.DefaultSize, self.serial_port_dict.values())
        sizer_option_2.Add(self.option_port, 0, wx.ALL, 5)

        # 设置波特率
        self.option_2_st2 = wx.StaticText(self.panel, wx.ID_ANY,
            u'波特率：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_2.Add(self.option_2_st2, 0, wx.ALL, 5)

        self.option_baudrate = wx.Choice(self.panel, wx.ID_ANY,
            wx.DefaultPosition, wx.DefaultSize, self.serial_baudrate_dict.values())
        sizer_option_2.Add(self.option_baudrate, 0, wx.ALL, 5)

        self.open_port_button = wx.Button(self.panel, wx.ID_ANY,
            u'打开串口', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_2.Add(self.open_port_button, 1, wx.ALL, 5)

        self.close_port_button = wx.Button(self.panel, wx.ID_ANY,
            u'关闭串口', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_2.Add(self.close_port_button, 1, wx.ALL, 5)

        #预留图标
        #self.static_png = wx.Image(r'res/unlink.png', wx.BITMAP_TYPE_PNG)
        #self.static_bitmap = wx.StaticBitmap(self.panel, wx.ID_ANY,
                #wx.BitmapFromImage(self.static_png),(16,16))

        #sizer_option_2.Add(self.static_bitmap, 0, wx.ALL, 5)
        #
        self.send_as_hex = wx.CheckBox(self.panel, wx.ID_ANY,
            u'HEX发送',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_2.Add(self.send_as_hex, 0, wx.ALL, 5)

        sizer_main.Add(sizer_option_2, 0, wx.ALL | wx.EXPAND, 5)

        # option 3
        sizer_option_3 = wx.BoxSizer(wx.HORIZONTAL)

        # 设置数据位
        self.option_3_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'数据位：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_3.Add(self.option_3_st1, 0, wx.ALL, 5)

        self.option_bytesize = wx.Choice(self.panel, wx.ID_ANY,
             wx.DefaultPosition, wx.DefaultSize, self.serial_bytesize_dict.values())
        sizer_option_3.Add(self.option_bytesize, 0, wx.ALL, 5)

        # 设置停止位
        self.option_3_st2 = wx.StaticText(self.panel, wx.ID_ANY,
            u'停止位：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_3.Add(self.option_3_st2, 0, wx.ALL, 5)

        self.option_stopbits = wx.Choice(self.panel, wx.ID_ANY,
             wx.DefaultPosition, wx.DefaultSize, self.serial_stopbits_dict.values())
        sizer_option_3.Add(self.option_stopbits, 0, wx.ALL, 5)

        # 设置校验位
        self.option_3_st3 = wx.StaticText(self.panel, wx.ID_ANY,
            u'校验位：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_3.Add(self.option_3_st3, 0, wx.ALL, 5)

        self.option_parity = wx.Choice(self.panel, wx.ID_ANY,
            wx.DefaultPosition, wx.DefaultSize,  self.serial_parity_dict.values())
        sizer_option_3.Add(self.option_parity, 0, wx.ALL, 5)

        self.help_button = wx.Button(self.panel, wx.ID_ANY,
            u'帮助', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_option_3.Add(self.help_button, 1, wx.ALL, 5)

        sizer_main.Add(sizer_option_3, 0, wx.ALL | wx.EXPAND, 5)

        # SetSizer
        self.panel.SetSizer(sizer_main)
        self.panel.Layout()
        sizer_main.Fit(self.panel)
        sizer_top.Add(self.panel, 1, wx.EXPAND |wx.ALL, 0)

        self.SetSizer(sizer_top)
        self.Layout()


        # 底部状态条
        self.CreateStatusBar(3)
        self.SetStatusWidths([-3,-4,-3])
        self.SetStatusText(u"    a python script",0)

        # 菜单条

        # 窗口居中
        self.Centre(wx.BOTH)

        # 绑定事件
        self.Bind(wx.EVT_BUTTON, self.OnClear, self.clear_button)
        self.Bind(wx.EVT_BUTTON, self.OnSend, self.send_button)
        self.Bind(wx.EVT_BUTTON, self.OnOpen_Port, self.open_port_button)
        self.Bind(wx.EVT_BUTTON, self.OnClose_Port, self.close_port_button)
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)

        self.option_port.SetSelection(1)
        self.option_baudrate.SetSelection(12)
        self.option_bytesize.SetSelection(3)
        self.option_stopbits.SetSelection(0)
        self.option_parity.SetSelection(0)

    def List_To_Dict(self, m_list):
            n_list = []
            for i in range(len(m_list)):
                n_list.append([i, str(m_list[i])])
            return dict(n_list)

        #wx.MessageBox(u'当前串口号被占用，或其它错误','ERROR', wx.OK | wx.ICON_EXCLAMATION)

    def OnSend(self, event):
        if not self.is_linking_to_port:
            return
        msg = self.send_text.GetValue()
        self.TX_count += len(msg)
        if not msg:
            return
        try:
            msg = msg.encode('gb18030')
        except Exception,e:
            print e
        else:
            print "Send: %s" %msg
            if self.send_as_hex.IsChecked():
                msg = binascii.b2a_hex(msg)
                print "Hex: %s" %msg
            self.serial.write(msg)
            self.SetStatusText(u"S:%d R:%d" %(self.TX_count,self.RX_count),2)

    def OnOpen_Port(self, event):
        try:
            self.serial.port =  self.option_port.GetSelection()
            self.serial.baudrate = int(self.serial_baudrate_dict[self.option_baudrate.GetSelection()])
            self.serial.bytesize = int(self.serial_bytesize_dict[self.option_bytesize.GetSelection()])
            self.serial.stopbits = int(self.serial_stopbits_dict[self.option_stopbits.GetSelection()])
            self.serial.parity = self.serial_parity_dict[self.option_parity.GetSelection()]
        except:
            self.OnClose_Port(self) # 不加参数会出错
        # 这里有1个BUG，判断isOpen()每次都为False
        if not self.is_linking_to_port:
            #print "open port"
            try:
                self.serial.open()
            except:
                self.SetStatusText(u"打开串口失败",2)
            else:
                print self.serial
                self.is_linking_to_port = True
                self.SetStatusText(u"已连接%s  [%s-%s-%s-%s]" % (self.serial_port_dict[self.option_port.GetSelection()],
                    self.serial.baudrate, self.serial.bytesize,self.serial.stopbits, self.serial.parity),1)
                self.SetStatusText(u"S:0 R:0", 2)
                self.task = Recv_Thread(self,self.serial)
                self.task.start()

    def OnClose_Port(self, event):
        #print self.serial
        if self.is_linking_to_port:
            self.task.stop()
            self.serial.close()
            self.is_linking_to_port = False
            self.SetStatusText(u"未连接串口",1)

    def OnClear(self, event):
        self.recv_text.Clear()
        self.send_text.Clear()

    def OnCloseWindow(self, event):
        try:
            self.serial.close()
        except:
            pass
        self.OnClose_Port(self)
        self.Destroy()

    def Update_Recv_Text(self, text):
        #if self.display_as_hex.IsChecked:
            #self.recv_text.AppendText(binascii.b2a_hex(text))
        #else:
            #text = ''.join([(c >= ' ') and c or '<%d>' % ord(c)  for c in text])
        text = text.replace('\r\n', '\n')
        text = text.replace('\r', '\n')
        self.RX_count += len(text)
        if self.display_as_hex.IsChecked():
            text = binascii.b2a_hex(text)
            print "Hex:%s" %text
            text = text.replace('0a', '\n') #还原16进制的\n
        self.recv_text.AppendText(text)
        self.SetStatusText(u"S:%d R:%d" %(self.TX_count,self.RX_count),2)

class Recv_Thread(threading.Thread):
    def __init__(self, window, ser):
        threading.Thread.__init__(self)
        self.window = window
        self.serial = ser
        self.time_to_quit = threading.Event()
        self.time_to_quit.clear()

    def stop(self):
        self.time_to_quit.set()

    def run(self):
        #print "run"
        while self.serial.isOpen():
            #print "ok"
            if self.time_to_quit.isSet():
                break
            text = self.serial.read(1)  #读一个字符
            if text:
                n = self.serial.inWaiting() #检查是否还有未接收的字符
                if n:
                    text = text + self.serial.read(n)
                print "Recv:%s" %text
                wx.CallAfter(self.window.Update_Recv_Text, text)
        print "stop"

if __name__ == '__main__':
    app = wx.App(False)
    window_main = WinMain()
    window_main.Show()
    app.MainLoop()
