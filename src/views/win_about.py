#!/usr/bin/env python
# -*- coding: utf-8 -*-

#import wx
import wx.html

class WinAbout(wx.Dialog):

    def __init__(self):
        wx.Dialog.__init__(self, wx.GetApp().TopWindow, wx.ID_ANY,
                 u"About", wx.DefaultPosition, wx.Size(550,350), wx.DEFAULT_DIALOG_STYLE )
        
        favicon = wx.Icon(r'res/favicon.ico', wx.BITMAP_TYPE_ICO, 16, 16)
        self.SetIcon(favicon)
        
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        
        sizer_top = wx.BoxSizer(wx.VERTICAL)       
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        sizer_main = wx.BoxSizer(wx.VERTICAL)
        
        self.html_panel = wx.html.HtmlWindow( self.panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.html.HW_SCROLLBAR_AUTO | wx.SUNKEN_BORDER )
        self.html_panel.LoadPage('res/about.htm')
        sizer_main.Add(self.html_panel, 1, wx.ALL | wx.EXPAND, 5)
        
        self.panel.SetSizer( sizer_main )
        self.panel.Layout()
        sizer_main.Fit(self.panel)
        sizer_top.Add(self.panel, 1, wx.EXPAND, 0)
        
        self.SetSizer(sizer_top)
        self.Layout()
        
        self.Centre(wx.BOTH)
        
    def __del__(self):
        self.Close()
        