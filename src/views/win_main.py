#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import wx
import views.win_about
import views.win_option
#import serial
class WinMain(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, u"cgf's com shell",
            wx.DefaultPosition, wx.Size(500, 400), wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        favicon = wx.Icon(r'res/favicon.ico', wx.BITMAP_TYPE_ICO, 16, 16)
        self.SetIcon(favicon)
        
        self.threads = []
        self.ports = ['COM1','COM2','COM3']
        self.port_dict = {}
        self.port_number = ""
        self.find_no_port = True
        self.is_linking_to_port = False

        # 得出可用COM端
        #self.serial_ports = self.Scan()
        #if not self.serial_ports:
            #pass
        #else:
            #self.find_no_port = False
            #self.port_dict = dict(self.serial_ports)
            #self.ports = ["%s" %i[0] for i  in self.serial_ports]

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        sizer_top = wx.BoxSizer(wx.VERTICAL)
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        sizer_main = wx.BoxSizer(wx.VERTICAL)

        # 布置组件

        # 接收显示
        self.recv_text = wx.TextCtrl(self.panel, wx.ID_ANY,
            u"",wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE)
        sizer_main.Add(self.recv_text, 1, wx.ALL | wx.EXPAND, 5)

        self.option_button = wx.Button(self.panel, wx.ID_ANY,
            u'设置', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_main.Add(self.option_button, 0, wx.ALL, 5)


        self.about_button = wx.Button(self.panel, wx.ID_ANY,
            u'关于', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_main.Add(self.about_button, 0, wx.ALL, 5)


        # SetSizer
        self.panel.SetSizer(sizer_main)
        self.panel.Layout()
        sizer_main.Fit(self.panel)
        sizer_top.Add(self.panel, 1, wx.EXPAND |wx.ALL, 0)

        self.SetSizer(sizer_top)
        self.Layout()


        # 底部状态条
        self.CreateStatusBar(3)
        self.SetStatusWidths([-3,-4,-3])
        self.SetStatusText(u"    a python script",0)
        if self.find_no_port:
            self.SetStatusText(u"未发现串口",2)

        # 菜单条

        # 窗口居中
        self.Centre(wx.BOTH)

        # 绑定事件
        self.Bind(wx.EVT_BUTTON, self.On_About, self.about_button)
        self.Bind(wx.EVT_BUTTON, self.On_Option, self.option_button)
        self.Bind(wx.EVT_CLOSE, self.OnExit)
   
    def On_About(self, event):
        app = wx.App(False)
        
        window_about = views.win_about.WinAbout()
        window_about.Show()
        
        app.MainLoop()
        
    def On_Option(self, event):
        app = wx.App(False)
        
        window_about = views.win_option.WinOption()
        window_about.Show()
        
        app.MainLoop()
       
    def OnExit(self, event):
        self.Destroy()
        sys.exit()
         
    def __del__(self):
        self.Destroy()
        sys.exit()

