#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
import serial
#import sqlite3

class WinOption(wx.Frame):
    
    def __init__(self):
        wx.Frame.__init__(self, wx.GetApp().TopWindow, wx.ID_ANY, u'Options', 
            wx.DefaultPosition,  wx.Size(500,350), 
            wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        
        self.find_no_port = True
        self.port_dict = {}
        # 串口号默认显示为COM1
        self.ports = ['COM1']
        # 得出可用COM端
        self.serial_ports = self.Scan()
        if not self.serial_ports:
            pass
        else:
            self.find_no_port = False
            self.port_dict = dict(self.serial_ports)
            self.ports = ["%s" %i[0] for i  in self.serial_ports]
        
        favicon = wx.Icon('res/favicon.ico', wx.BITMAP_TYPE_ICO, 16, 16)
        self.SetIcon(favicon)
        
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        sizer_top = wx.BoxSizer(wx.VERTICAL)
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        sizer_main = wx.BoxSizer(wx.VERTICAL)
        
        # general option 
        sbsizer_general = wx.StaticBoxSizer(wx.StaticBox(self.panel, wx.ID_ANY, u"General"), wx.VERTICAL)

        # 设置串口号
        sizer_general_1 = wx.BoxSizer(wx.HORIZONTAL)
        self.general_1_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'串口号：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_general_1.Add(self.general_1_st1, 0, wx.ALL, 5)

        self.general_port = wx.ComboBox(self.panel, wx.ID_ANY,
            'COM1', wx.DefaultPosition, wx.DefaultSize, self.ports,
                        wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER | wx.CB_READONLY)
        sizer_general_1.Add(self.general_port, 0, wx.ALL, 5)
        sbsizer_general.Add(sizer_general_1, 1, wx.ALL | wx.EXPAND, 5)
         
        # 设置波特率
        sizer_general_2 = wx.BoxSizer(wx.HORIZONTAL)
        self.general_2_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'波特率：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_general_2.Add(self.general_2_st1, 0, wx.ALL, 5)

        self.general_baudrate = wx.ComboBox(self.panel, wx.ID_ANY,
            '9600', wx.DefaultPosition, wx.DefaultSize,
            ['110','300','600','1200','2400','4800','9600',
            '14400','19200','38400','56000','57600','115200','128000','256000'],
            wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER | wx.CB_READONLY)
        sizer_general_2.Add(self.general_baudrate, 0, wx.ALL, 5)
        sbsizer_general.Add(sizer_general_2, 1, wx.ALL | wx.EXPAND, 5)

        # 设置数据位
        sizer_general_3 = wx.BoxSizer(wx.HORIZONTAL)
        self.general_3_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'数据位：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_general_3.Add(self.general_3_st1, 0, wx.ALL, 5)

        self.general_bytesize = wx.ComboBox(self.panel, wx.ID_ANY,
            '8', wx.DefaultPosition, wx.DefaultSize, ['5','6','7','8'],
            wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER | wx.CB_READONLY)
        sizer_general_3.Add(self.general_bytesize, 0, wx.ALL, 5)
        sbsizer_general.Add(sizer_general_3, 1,  wx.ALL | wx.EXPAND, 5)
        
        # 设置停止位
        sizer_general_4 = wx.BoxSizer(wx.HORIZONTAL)
        self.general_4_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'停止位：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_general_4.Add(self.general_4_st1, 0, wx.ALL, 5)

        self.general_stopbits = wx.ComboBox(self.panel, wx.ID_ANY,
            '1', wx.DefaultPosition, wx.DefaultSize, ['1','1.5','2'],
            wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER | wx.CB_READONLY)
        sizer_general_4.Add(self.general_stopbits, 0, wx.ALL, 5)
        sbsizer_general.Add(sizer_general_4, 1, wx.ALL | wx.EXPAND, 5)
        

        # 设置校验位
        sizer_general_5 = wx.BoxSizer(wx.HORIZONTAL)
        self.general_5_st1 = wx.StaticText(self.panel, wx.ID_ANY,
            u'校验位：',
            wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_general_5.Add(self.general_5_st1, 0, wx.ALL, 5)

        self.general_parity = wx.ComboBox(self.panel, wx.ID_ANY,
            'None', wx.DefaultPosition, wx.DefaultSize, ['None','Odd','Even','Mark','Space'],
            wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER | wx.CB_READONLY)
        sizer_general_5.Add(self.general_parity, 0, wx.ALL, 5)
        sbsizer_general.Add(sizer_general_5, 1, wx.ALL | wx.EXPAND, 5)

        sizer_main.Add(sbsizer_general, 1, wx.ALL | wx.EXPAND, 5)
        
        # 按钮
        sizer_button = wx.BoxSizer(wx.HORIZONTAL)
        self.save_button = wx.Button(self.panel, wx.ID_ANY,
            u'保存', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_button.Add(self.save_button, 1, wx.ALL, 5)
        
        self.cancel_button = wx.Button(self.panel, wx.ID_ANY,
            u'取消', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_button.Add(self.cancel_button, 1, wx.ALL, 5)
        
        sizer_main.Add(sizer_button, 0, wx.ALL | wx.EXPAND, 5)
             
        self.panel.SetSizer(sizer_main)
        self.panel.Layout()
        sizer_main.Fit(self.panel)
        sizer_top.Add(self.panel, 1, wx.EXPAND | wx.ALL, 0)

        self.SetSizer(sizer_top)
        self.Layout()

        self.Centre(wx.BOTH)
        self.Bind(wx.EVT_BUTTON, self.OnCancel,self.cancel_button)
        self.Bind(wx.EVT_BUTTON, self.OnSave,self.save_button)
        
    def Scan(self):
        available = []
        for i in range(256):
            try:
                s = serial.Serial(i)
                available.append((s.portstr,i))
                s.close()
                #break
            except serial.SerialException:
                pass
        return available
        
    def OnSave(self, event):
        # port = self.general_port.GetValue()
        pass
        
        
    def OnCancel(self,event):
        self.Close()
        
    def __del__(self):
        self.Close()
