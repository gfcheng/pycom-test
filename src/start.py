#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 2011-10-29

@author: goldfilm(at)gmail.com
'''
import wx
import views.win_main
    
if __name__ == '__main__':
    app = wx.App(False)
   
    window_main = views.win_main.WinMain()
    window_main.Show()
   
    app.MainLoop()
