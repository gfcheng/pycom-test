# coding:utf-8
# 测试主线程和子线程的通信
# 开中断关闭子线程
import sys
import threading
import wx
import time

class WinMain(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, u"win_main.py",
            wx.DefaultPosition, wx.Size(300, 300), wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        sizer_top = wx.BoxSizer(wx.VERTICAL)
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        sizer_main = wx.BoxSizer(wx.HORIZONTAL)

        sizer_button = wx.BoxSizer(wx.VERTICAL)
        self.open_port_button = wx.Button(self.panel, wx.ID_ANY,
            u'打开串口', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_button.Add(self.open_port_button, 1, wx.ALL, 5)

        self.close_port_button = wx.Button(self.panel, wx.ID_ANY,
            u'关闭串口', wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_button.Add(self.close_port_button, 1, wx.ALL, 5)
        sizer_main.Add(sizer_button, 0, wx.EXPAND |wx.ALL, 0)

        self.recv_text = wx.TextCtrl(self.panel, wx.ID_ANY,
            u"",wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE)
        sizer_main.Add(self.recv_text, 1, wx.ALL | wx.EXPAND, 5)
        # SetSizer
        self.panel.SetSizer(sizer_main)
        self.panel.Layout()
        sizer_main.Fit(self.panel)
        sizer_top.Add(self.panel, 1, wx.EXPAND |wx.ALL, 0)

        self.SetSizer(sizer_top)
        self.Layout()

        self.Centre(wx.BOTH)

        # 绑定事件
        self.Bind(wx.EVT_CLOSE, self.OnExit)
        self.Bind(wx.EVT_BUTTON, self.OnOpen, self.open_port_button)
        self.Bind(wx.EVT_BUTTON, self.OnClose, self.close_port_button)

    def OnOpen(self, event):
        self.task = Work_Thread(self, 10)
        self.task.start()

    def OnClose(self, event):
        self.task.stop()
        self.task.join()
    def OnExit(self, event):
        self.Destroy()
        sys.exit()

    def Update_Recv_Text(self, text):
        self.recv_text.AppendText(text)

class Work_Thread(threading.Thread):
    def __init__(self, window, num):
        threading.Thread.__init__(self)
        self.window = window
        self.num = num
        self.time_to_quit = threading.Event()
        self.time_to_quit.clear()
    def stop(self):
        self.time_to_quit.set()
    def run(self):
        print "run"
        print self.time_to_quit.isSet()
        while self.num:
            print self.num
            wx.CallAfter(self.window.Update_Recv_Text, str(self.num)+"\n")
            self.num -= 1
            if self.time_to_quit.isSet():
                print "interrupt"
                break
            time.sleep(1)
        print "run over"

if __name__ == '__main__':
    app = wx.App(False)

    window_main = WinMain()
    window_main.Show()

    app.MainLoop()